# Go Parallel Crawler

Round-based parallel crawler written in Go.

The function `Crawl()` starts from a set of URLs (in general, pointers); visits
the web pages (in general, resources) they point to, i.e., it fetches them and
does something with the response; combines the URLs found to create a new set of
URLs to be visited during the next round; repeats. Each URL is visited only
once. To do so, it leverages two interfaces: `Fetcher` and `Doer`.

- A `Fetcher` fetches a resource given a pointer. It also establishes the policy
  that determines which pointers have to be visited during next round. `Fetch()`
  returns a Reader for the fetched resource and the set of relevant pointers it
  contains.
- A `Doer` does something with a fetched resource. `Do()` takes a pointer and a
  Reader and returns an error. To use results you have to store them somewhere,
  leveraging a shared data structure.

For example, assume you want to know the titles of all Wikipedia pages titles
linked recursively by a starting page. The `Fetcher` will have to: GET the page,
parse it to find `<a>` tags, filter out the links that point outside of
Wikipedia or to a Wikipedia page that is not a true encyclopedia article. The
`Doer` will have to: parse the HTML page, extract the text inside `<h1
id="firstHeading">`, store it somewhere (or print it).

Useful resources:

- Concurrency:
  - [Handling 1 Million Requests per Minute](http://marcio.io/2015/07/handling-1-million-requests-per-minute-with-golang/)
  - [Stackoverflow - Pool of goroutines](https://stackoverflow.com/questions/18267460/how-to-use-a-goroutine-pool)
  - [Go by Example - Worker pools](https://gobyexample.com/worker-pools)
- Logs:
  - [logrus](https://github.com/sirupsen/logrus)
  - [logur](https://github.com/logur/logur)
  - [adapter-logrus](https://github.com/logur/adapter-logrus)
  - [Modern Go Application](https://github.com/sagikazarmark/modern-go-application/blob/main/cmd/modern-go-application/main.go)
- Generics and interfaces:
  - https://stackoverflow.com/questions/44370277/type-is-pointer-to-interface-not-interface-confusion
  - https://stackoverflow.com/questions/72034479/how-to-implement-generic-interfaces
  - https://stackoverflow.com/questions/27775376/value-receiver-vs-pointer-receiver

Nice to have:

- telemetry
- tests
- graceful degradation
