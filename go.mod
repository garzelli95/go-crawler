module gitlab.com/garzelli95/go-crawler

go 1.19

require (
	github.com/sirupsen/logrus v1.9.0
	golang.org/x/net v0.0.0-20220906165146-f3363e06e74c
)

require logur.dev/logur v0.16.1 // indirect

require (
	golang.org/x/sys v0.0.0-20220728004956-3c1f35247d10 // indirect
	logur.dev/adapter/logrus v0.5.0
)
