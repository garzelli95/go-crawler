package main

import (
	"fmt"
	"net/http"
	"os"
	"time"

	"gitlab.com/garzelli95/go-crawler/internal/pkg/jobs"
	"gitlab.com/garzelli95/go-crawler/pkg/crawl"
	"gitlab.com/garzelli95/go-crawler/pkg/link"
)

func main() {
	// CalculatorExample()
	CrawlWeb()
}

func CrawlWeb() {
	// crawl.Crawl([]string{"http://google.it"}, 2, crawl.HttpFetcher{}, crawl.LogDoer{})
	crawl.Crawl([]string{"http://google.it"}, 3, crawl.HTTPFetcher{}, crawl.NoopDoer{})
}

func CrawlWithLocalFileFetcher() {
	lff := crawl.LocalHTMLFetcher{BasePath: "test/fetcher"}
	crawl.Crawl([]string{"root"}, 2, lff, crawl.NoopDoer{})
}

func ParseRemoteHTML() {
	// parse html from remote url
	url := "https://github.com/golang-standards/project-layout/tree/master/test"
	resp, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	// get links
	urls, err := link.Parse(resp.Body)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%v\n", urls)
}

func ParseLocalFile() {
	// parse html from local file
	r, err := os.Open("test/example.html")
	if err != nil {
		panic(err)
	}
	defer r.Close()

	// get links
	urls, err := link.Parse(r)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%v\n", urls)
}

func UseLocalFileFetcher() {
	lff := crawl.LocalHTMLFetcher{BasePath: "test/fetcher"}
	_, paths, err := lff.Fetch("root")
	if err != nil {
		panic(err)
	}
	fmt.Printf("%v\n", paths)
}

func UseCalculator() {

	// define required types and functions
	type AvgPartial struct {
		Sum, Count int
	}
	sumAndCount := func(s []int) AvgPartial {
		sum := 0
		for _, v := range s {
			sum += v
		}
		time.Sleep(time.Second)
		return AvgPartial{Sum: sum, Count: len(s)}
	}
	avg := func(all ...[]AvgPartial) float64 {
		totSum := 0.0
		totCount := 0
		for _, s := range all {
			for _, p := range s {
				totSum += float64(p.Sum)
				totCount += p.Count
			}
		}

		return totSum / float64(totCount)
	}

	// (large amount of) input data
	data := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16}
	fmt.Printf("data: %v\n", data)

	// parallelize work
	var calculator jobs.Calculator[[]int, AvgPartial] = jobs.NewPoolCalculator(sumAndCount, 2, 3, 4)
	numJobs := 4
	for i := 0; i < numJobs; i++ {
		q := len(data) / numJobs
		l := i * q
		h := l + q
		// executor.Submit(data[l:h])
		if i < 0 {
			// submit now
			calculator.Submit(data[l:h])
		} else {
			// submit later
			go func(s []int) {
				time.Sleep(time.Second)
				calculator.Submit(s)
			}(data[l:h])
		}
	}

	partialResults1 := calculator.Collect()
	fmt.Printf("first partial results: %v\n", partialResults1)

	time.Sleep(3 * time.Second)

	partialResults2 := calculator.Collect()
	fmt.Printf("remaining partial results: %v\n", partialResults2)

	// aggregate partial results
	fmt.Printf("avg: %v\n", avg(partialResults1, partialResults2))

	fmt.Printf("3rd partial results: %v\n", calculator.Collect())
}

func UseExecutor() {
	print := func(s []int) {
		fmt.Printf("%v\n", s)
	}

	var executor jobs.Executor[[]int] = jobs.NewPoolExecutor(print, 2, 4)
	executor.Submit([]int{11, 22, 44, 88})
	executor.Submit([]int{33, 55, 77, 99})
	executor.Submit([]int{1, 2, 3, 4, 5})
	executor.Await()
}

func CalculatorExample() {
	type AvgPartial struct {
		Sum, Count int
	}
	sumAndCount := func(s []int) AvgPartial {
		fmt.Printf("Summing and counting %v\n", s)
		sum := 0
		for _, v := range s {
			sum += v
		}
		return AvgPartial{Sum: sum, Count: len(s)}
	}

	fmt.Println("--- Example usage ---")
	var ex jobs.Calculator[[]int, AvgPartial] = jobs.NewPoolCalculator(sumAndCount, 1, 2, 4)
	ex.Submit([]int{0, 0, 0, 0})
	ex.Submit([]int{1})
	ex.Submit([]int{2, 2})
	ex.Submit([]int{3, 3, 3, 3})
	ex.Await()
	ex.Shutdown()
	res := ex.Collect()
	fmt.Printf("Collected results: %v\n", res)
}
