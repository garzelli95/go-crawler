package jobs

import "sync"

type Task[I any] func(I)

type Executor[I any] interface {
	Submit(request I)
	Await()
	Shutdown()
}

type PoolExecutor[I any] struct {
	jobs chan I // to receive job requests and dispatch them to a free worker
	wg   *sync.WaitGroup
}

// NewPoolExecutor creates a new Executor with a job queue and a fixed-size
// workers pool. Workers live in separate goroutines.
func NewPoolExecutor[I any](job Task[I], poolSize, jobQueueCap int) PoolExecutor[I] {

	e := PoolExecutor[I]{
		jobs: make(chan I, jobQueueCap),
		wg:   &sync.WaitGroup{},
	}

	// create worker pool
	for i := 0; i < poolSize; i++ {
		spawnDoer(job, e.jobs, e.wg)
	}

	return e
}

// spawnDoer creates a worker in a new goroutine. A worker receives inputs from
// a channel and executes a job.
func spawnDoer[I any](job Task[I], requests <-chan I, wg *sync.WaitGroup) {
	// execute executes a job
	execute := func(input I) {
		defer wg.Done()
		job(input)
	}

	// a worker receives a job's request and executes it, then repeats
	worker := func() {
		for input := range requests {
			execute(input)
		}
	}

	go worker()
}

// Submit adds a job requst to the job queue. It blocks if the queue is full.
func (e PoolExecutor[I]) Submit(req I) {
	e.wg.Add(1)
	e.jobs <- req // send job request to job queue
}

// Await blocks until all jobs requests are executed.
func (e PoolExecutor[I]) Await() {
	e.wg.Wait()
}

// Shutdown frees a PoolExecutor's resources. No more jobs are accepted, but
// existing ones will continue. The Executor cannot be reused after this method.
func (e PoolExecutor[I]) Shutdown() {
	close(e.jobs)
}
