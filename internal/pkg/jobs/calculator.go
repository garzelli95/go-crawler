package jobs

import (
	"sync"
	"sync/atomic"
)

type Computation[I, R any] func(I) R

type Calculator[I, R any] interface {
	Executor[I]
	Collect() []R
}

type PoolCalculator[I, R any] struct {
	jobs    chan I // to receive job requests and dispatch them to a free worker
	results chan R // channel to return job results

	wg *sync.WaitGroup

	submitted int32 // atomic counter for submitted jobs
	returned  int32 // atomic counter for job results returned
}

// NewPoolCalculator creates a new Executor for executing computations of the
// type specified as argument, with a job queue, a results queue, and a workers
// pool. Workers live in separate goroutines.
func NewPoolCalculator[I, R any](job Computation[I, R], poolSize, jobQueueCap, resultsCap int) *PoolCalculator[I, R] {

	c := &PoolCalculator[I, R]{
		jobs:    make(chan I, jobQueueCap),
		results: make(chan R, resultsCap),
		wg:      &sync.WaitGroup{},
	}

	// create worker pool
	for i := 0; i < poolSize; i++ {
		spawnWorker(job, c.jobs, c.results, c.wg)
	}

	return c
}

// spawnWorker creates a worker in a new goroutine. A worker receives inputs
// from a channel, executes a job, sends job's results to an output channel and
// repeats (until the input channel is closed). A worker could block if the
// results queue were full.
func spawnWorker[I, R any](job Computation[I, R], requests <-chan I, results chan<- R, wg *sync.WaitGroup) {
	// execute executes a job
	execute := func(input I) {
		defer wg.Done()
		results <- job(input) // TODO check if results queue is full and in case store for later
	}

	// a worker receives a job's request and executes it, then repeats
	worker := func() {
		for input := range requests {
			execute(input)
		}
	}
	go worker()
}

// Submit adds a job requst to the job queue. It blocks if the queue is full.
func (c *PoolCalculator[I, R]) Submit(req I) {
	c.wg.Add(1)
	atomic.AddInt32(&c.submitted, 1) // increment submitted jobs counter
	c.jobs <- req                    // send job request to job queue
}

// Collect returns all(*) computed results in a slice.
//
// (*) Only jobs whose Submit() operation happened before the execution of this
// method count. If Submit() operations were executed in different subroutines,
// they may not got there when this method was executed, meaning that their
// result would remain in the channel. In case of sequential (single-thread)
// invocations to Submit()s and Collect() there are no problems.
func (c *PoolCalculator[I, R]) Collect() []R {
	ret := make([]R, 0, (c.submitted*3/2)+1)

	nReturned := int(atomic.LoadInt32(&c.returned))   // results returned since the beginning
	nSubmitted := int(atomic.LoadInt32(&c.submitted)) // jobs submitted since the beginning
	var nReceived int                                 // results received during this function execution

	// check every time the current value of c.submitted (atomically)
	for nReturned < nSubmitted {

		// optimistic loop
		for nReceived = 0; nReturned+nReceived < nSubmitted; nReceived++ {
			ret = append(ret, <-c.results)
		}

		atomic.AddInt32(&c.returned, int32(nReceived))

		// step for next iteration
		nReturned = int(atomic.LoadInt32(&c.returned))
		nSubmitted = int(atomic.LoadInt32(&c.submitted))
	}

	return ret
}

// Await blocks until all jobs requests are executed.
func (c *PoolCalculator[I, R]) Await() {
	c.wg.Wait()
}

// Shutdown frees Calculator resources. No more jobs are accepted, but existing
// ones will continue and return their result eventually. The Calculator cannot
// be reused after this method.
func (c *PoolCalculator[I, R]) Shutdown() {
	// accept no more jobs => close job queue => make workers stop
	close(c.jobs)
}
