// Logging package with just the functions that I am going to use. Currently it
// is not compatible with the default log "interaface", but you just need to add
// functions here. Under the hood, it uses logrus.
package log

import (
	"os"

	"github.com/sirupsen/logrus"
	logrusadapter "logur.dev/adapter/logrus"
	"logur.dev/logur"
)

var std logur.Logger = NewDefaultLogger()

// NewDefaultLogger creates a new logger.
func NewDefaultLogger() logur.Logger {
	mylogger := logrus.New()

	mylogger.SetOutput(os.Stdout)
	mylogger.SetFormatter(&logrus.JSONFormatter{})
	mylogger.SetLevel(logrus.TraceLevel)

	return logrusadapter.New(mylogger)
}

func Trace(msg string, fields ...map[string]interface{}) {
	std.Trace(msg, fields...)
}
func Debug(msg string, fields ...map[string]interface{}) {
	std.Debug(msg, fields...)
}
func Info(msg string, fields ...map[string]interface{}) {
	std.Info(msg, fields...)
}
func Warn(msg string, fields ...map[string]interface{}) {
	std.Warn(msg, fields...)
}
func Error(msg string, fields ...map[string]interface{}) {
	std.Error(msg, fields...)
}
