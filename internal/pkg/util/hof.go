package util

// Map returns a new slice where each element is the output of f(item).
func Map[T, V any](items []T, f func(T) V) []V {
	ret := make([]V, len(items))

	for i, item := range items {
		ret[i] = f(item)
	}

	return ret
}

// Filter returns a new slice with only the items that make keep() return true.
func Filter[T any](items []T, keep func(T) bool) []T {
	ret := make([]T, 0, len(items))

	for _, item := range items {
		if keep(item) {
			ret = append(ret, item)
		}
	}

	return ret
}

// Unique returns a new slice with no duplicates. It takes O(n).
func Unique[T comparable](items []T) []T {
	keys := make(map[T]bool, len(items))
	ret := make([]T, 0, len(items))

	for _, item := range items {
		if _, found := keys[item]; !found {
			keys[item] = true
			ret = append(ret, item)
		}
	}

	return ret
}

// ForEach applies f() to each item in the slice. It returns the slice passed as
// argument.
func ForEach[T any](items []T, f func(T)) []T {

	for _, item := range items {
		f(item)
	}

	return items
}

// Update invokes f() on each item of a slice, updating it in place. It returns
// the slice passed as argument.
func Update[T any](items []T, f func(T) T) []T {

	for i, item := range items {
		items[i] = f(item)
	}

	return items
}
