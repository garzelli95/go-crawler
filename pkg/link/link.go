package link

import (
	"errors"
	"io"

	"golang.org/x/net/html"
)

// Parse reads html and returns a slice with all href values of <a> tags.
func Parse(r io.Reader) ([]string, error) {
	doc, err := html.Parse(r)
	if err != nil {
		return nil, err
	}
	urls := getAnchorHrefs(doc)
	return urls, nil
}

func getAnchorHrefs(doc *html.Node) []string {
	urls := make([]string, 0)

	var walk func(*html.Node)
	walk = func(node *html.Node) {
		// if node is <a>, then look for href and add it
		if node.Type == html.ElementNode && node.Data == "a" {
			href, err := extractHref(node)
			if err == nil {
				urls = append(urls, href)
			}
		}
		// repeat in subtrees
		for c := node.FirstChild; c != nil; c = c.NextSibling {
			walk(c)
		}
	}
	walk(doc)

	return urls
}

func extractHref(a *html.Node) (string, error) {
	for _, attr := range a.Attr {
		if attr.Key == "href" {
			return attr.Val, nil
		}
	}
	return "", errors.New("<a> without href")
}
