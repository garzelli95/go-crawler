package crawl

import (
	"io"
	"sync"

	"gitlab.com/garzelli95/go-crawler/internal/pkg/jobs"
	"gitlab.com/garzelli95/go-crawler/internal/pkg/log"
	"gitlab.com/garzelli95/go-crawler/internal/pkg/util"
)

type Fetcher interface {
	// Fetch returns a reader for the response body and a slice of URLs found on
	// the requested page. Fetch has the responsability of deciding which URLs
	// have to be returned to the caller. For example, it may decide to remove
	// duplicates or to filter URLs that differ just from a trailing backslash.
	Fetch(url string) (resp io.Reader, urls []string, err error)
}

type Doer interface {
	// Do is the operation to be performed on each resource pointed by `url` and
	// whose response body can be read through `r`.
	Do(url string, r io.Reader) error
}

type NoopDoer struct{}

func (d NoopDoer) Do(_ string, _ io.Reader) error {
	return nil
}

type LogDoer struct{}

func (d LogDoer) Do(url string, _ io.Reader) error {
	log.Trace(url)
	return nil
}

func Crawl(startURLs []string, maxDepth int, fetcher Fetcher, doer Doer) {
	visited := &sync.Map{}     // map: url -> fetched urls
	urlsThisRound := startURLs // set of urls to visit at this round

	// for each round until the maximum allowed depth
	for round := 1; round <= maxDepth; round++ {
		log.Debug("start visiting URLs", map[string]interface{}{
			"x_round":         round,
			"x_starting_urls": len(urlsThisRound),
		})
		visitAll(urlsThisRound, fetcher, doer, visited)

		log.Debug("start combining fetched URLs")
		urlsNextRound := combineAll(urlsThisRound, visited)

		log.Debug("end round", map[string]interface{}{
			"x_round":      round,
			"x_found_urls": len(urlsNextRound),
		})

		// next round
		urlsThisRound = urlsNextRound
	}
}

func visitAll(urlsThisRound []string, fetcher Fetcher, doer Doer, visited *sync.Map) {
	// define job input
	type visitInput struct {
		url     string
		fetcher Fetcher
		doer    Doer
		visited *sync.Map
	}

	// define job operation
	visitJob := func(i visitInput) error {
		return visitOne(i.url, i.fetcher, i.doer, i.visited)
	}

	// create executor for this type of jobs
	maxJobs := len(urlsThisRound)
	poolSize := util.Min(maxJobs, 16)
	var calculator jobs.Calculator[visitInput, error] = jobs.NewPoolCalculator(visitJob, poolSize, maxJobs, maxJobs)
	defer calculator.Shutdown()

	// visit each url
	for _, url := range urlsThisRound {
		// do nothing if URL has been already visited
		if _, found := visited.Load(url); found {
			continue
		}
		calculator.Submit(visitInput{
			url:     url,
			fetcher: fetcher,
			doer:    doer,
			visited: visited,
		})
	}

	// wait for URLs to be fectched and stuff to be done
	calculator.Await()

	// collect and log errors during jobs execution
	for _, err := range calculator.Collect() {
		if err != nil {
			log.Error(err.Error())
		}
	}
}

// visit fetches data from a URL and stores retrieved information in the map.
func visitOne(url string, fetcher Fetcher, doer Doer, visited *sync.Map) error {

	// exit if url has been already visited
	if _, found := visited.Load(url); found {
		return nil
	}

	// fetch data
	resp, urls, err := fetcher.Fetch(url)
	if err != nil {
		return err
	}

	// store fetched urls in the map and do something
	_, loaded := visited.LoadOrStore(url, urls)
	if stored := !loaded; stored {
		// first goroutine to store url => do something with fetched data
		return doer.Do(url, resp)
	}

	return nil
}

// combineAll takes the set of URLs used as starting points in this round and a
// map that links a URL to fetched URLs and computes the starting URLs for the
// next round.
func combineAll(urlsThisRound []string, visited *sync.Map) []string {
	urlsNextRound := make([]string, 0, len(urlsThisRound)*30)
	for _, url := range urlsThisRound {
		// retrieved URLs contained in the page pointed by the current URL
		loaded, found := visited.Load(url)
		if !found {
			continue
		}
		fetchedURLs := loaded.([]string)

		// ignore URLs already visited
		urlsYetToBeVisited := util.Filter(fetchedURLs, func(u string) bool {
			_, present := visited.Load(u) // present in map <=> already visited
			return !present               // keep if not present in the map
		})

		// combine all URLs parsed during this round
		urlsNextRound = append(urlsNextRound, urlsYetToBeVisited...)
	}

	// remove duplicates
	urlsNextRound = util.Unique(urlsNextRound)

	return urlsNextRound
}
