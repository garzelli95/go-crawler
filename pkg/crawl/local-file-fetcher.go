package crawl

import (
	"bytes"
	"io"
	"os"
	"path"

	"gitlab.com/garzelli95/go-crawler/pkg/link"
)

type LocalHTMLFetcher struct {
	BasePath string
}

func (lff LocalHTMLFetcher) Fetch(p string) (io.Reader, []string, error) {
	// open html file
	file := path.Join(lff.BasePath, p+".html") // we assume paths are joinable
	f, err := os.Open(file)
	if err != nil {
		return nil, nil, err
	}
	defer f.Close()

	// Trick to read twice from the same reader: create a TeeReader and a
	// Buffer. Read from TeeReader first! As you read from the original reader
	// `f`, you also write to the buffer. In this way you have two readers: tee
	// (`r1`) and buf (`r2`).
	r2 := &bytes.Buffer{}
	r1 := io.TeeReader(f, r2)

	// parse html file to get "urls"
	paths, err := link.Parse(r1)
	if err != nil {
		return nil, nil, err
	}

	return r2, paths, nil
}
