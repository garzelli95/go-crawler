package crawl

import (
	"bytes"
	"io"
	"net/http"
	"net/url"
	"path"
	"strings"

	"gitlab.com/garzelli95/go-crawler/internal/pkg/log"
	"gitlab.com/garzelli95/go-crawler/internal/pkg/util"
	"gitlab.com/garzelli95/go-crawler/pkg/link"
)

type HTTPFetcher struct {
}

func (hf HTTPFetcher) Fetch(aURL string) (io.Reader, []string, error) {
	// GET url
	resp, err := http.Get(aURL)
	if err != nil {
		return nil, nil, err
	}
	defer resp.Body.Close()

	// trick to read the response twice
	r2 := &bytes.Buffer{}
	r1 := io.TeeReader(resp.Body, r2)

	// parse html file to get urls
	urls, err := link.Parse(r1)
	if err != nil {
		return nil, nil, err
	}

	// get base URL to normalize URLs
	baseURL := &url.URL{
		Scheme: resp.Request.URL.Scheme,
		Host:   resp.Request.URL.Host,
	}

	// normalize URLs and remove duplicates
	urls = normalizeURLs(urls, baseURL)
	urls = util.Unique(urls)

	return r2, urls, nil
}

// normalizeURLs formats all URLs found in a page according to a standard
// format, i.e., it makes them absolute. It ignores non-HTTP(S) links.
func normalizeURLs(urls []string, baseURL *url.URL) []string {
	ret := make([]string, 0, len(urls))

	for _, rawHref := range urls {
		cleanedURL, err := cleanURL(rawHref, baseURL)
		if err != nil {
			log.Debug("cannot clean URL", map[string]interface{}{
				"x_url":    rawHref,
				"x_reason": err.Error(),
			})
			continue
		}
		// ignore URL if scheme is not http(s)
		if cleanedURL.Scheme != "https" && cleanedURL.Scheme != "http" {
			continue
		}

		ret = append(ret, cleanedURL.String())
	}

	return ret
}

// cleanURL standardizes the format of an HTTP(S) URL. If is is relative, it
// makes it absolute by appending the path to the base URL. Do not use with
// non-HTTP(S) URLs.
func cleanURL(rawHref string, baseURL *url.URL) (*url.URL, error) {
	// remove leading and trailing whitespaces before parsing
	trimmedHref := strings.TrimSpace(rawHref)
	u, err := url.Parse(trimmedHref)
	if err != nil {
		return nil, err
	}

	// strip fragment from URL, if any
	u.Fragment = ""

	// make URL absolute
	var absURL *url.URL
	if u.IsAbs() {
		absURL = u
	} else {
		// make path part absolute to avoid "./" or "../" after http://host/
		absPath := path.Join("/", u.Path)
		absURL = baseURL.JoinPath(absPath) // add path after baseURL
		absURL.RawQuery = u.RawQuery       // add query after baseURL/path
	}

	// clean path part of the URL
	if absURL.Path != "" {
		absURL.Path = path.Clean(absURL.Path)
	}

	// convert URL back to string and return it
	return absURL, nil
}
